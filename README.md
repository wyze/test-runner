# Test Runner

> An app to run tests.

## Getting Started

- Clone the repo
- Run `npm install`
- Run `npm run watch`
- Browse to http://localhost:9090
- Enjoy

## License

Copyright (c) 2015 [Neil Kistner](//github.com/wyze)

Released under the MIT license. See [LICENSE](LICENSE) for details.
