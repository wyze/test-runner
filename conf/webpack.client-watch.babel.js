import webpack from 'webpack';
import config  from './webpack.config.babel';

const url = 'http://localhost:8080';

config.cache = true;
config.debug = true;
config.devtool = 'eval';

config.entry.unshift(
  `webpack-dev-server/client?${url}`,
  'webpack/hot/only-dev-server'
);

config.output.publicPath = `${url}/dist/`;
config.output.hotUpdateMainFilename = 'update/[hash]/update.json';
config.output.hotUpdateChunkFilename = 'update/[hash]/[id].update.js';

config.plugins = [
  new webpack.DefinePlugin({
    __CLIENT__:    true,
    __SERVER__:    false,
    'process.env': { BROWSER: JSON.stringify(true) }
  }),
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NoErrorsPlugin()
];

config.module = {
  preLoaders: [
    {
      test: /\.jsx?$/,
      exclude: /node_modules/,
      loader: 'eslint'
    }
  ],
  loaders: [
    {
      test: /\.jsx?$/,
      loader: 'react-hot!babel',
      exclude: /node_modules/
    },
    {
      test: /\.css$/,
      loader: 'style!css!postcss'
    }
  ]
};

config.devServer = {
  publicPath:  `${url}/dist/`,
  contentBase: '..',
  hot:         true,
  inline:      true,
  lazy:        false,
  quiet:       true,
  noInfo:      false,
  headers:     { 'Access-Control-Allow-Origin': '*' },
  stats:       { colors: true },
  host:        '0.0.0.0'
};

export default config;
