import webpack           from 'webpack';
import path              from 'path';

export default {
  target:  'web',
  cache:   false,
  context: __dirname,
  devtool: false,
  entry:   ['../src/scripts/main'],
  output:  {
    path:          path.join(__dirname, '../dist'),
    filename:      'client.js',
    chunkFilename: '[name].[id].js',
    publicPath:    'dist/'
  },
  plugins: [
    new webpack.DefinePlugin({ __CLIENT__: true, __SERVER__: false }),
    new webpack.DefinePlugin({ 'process.env': { NODE_ENV: '"production"' } }),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin()
  ],
  module:  {
    preLoaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'eslint'
      }
    ],
    loaders: [
      {
        test: /\.jsx?$/,
        loader: 'babel',
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        loader: 'style!css!postcss'
      }
    ]
  },
  postcss: [
    require('postcss-import'),
    require('postcss-nested'),
    require('cssnext')(),
    require('csswring')
  ],
  resolve: {
    modulesDirectories: [
      'src',
      'node_modules',
      'web_modules'
    ],
    extensions: ['', '.js', '.jsx']
  },
  node:    {
    __dirname: true,
    fs:        'empty'
  }
}
