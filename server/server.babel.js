import React            from 'react';
import Router           from 'react-router';
import { data, routes } from '../src/scripts/config';
import { Server }       from 'hapi';

/**
 * Define isomorphic constants.
 */
global.__CLIENT__ = false;
global.__SERVER__ = true;

const server = new Server();

server.connection({
  port: process.env.PORT || 9090,
  uri:  'localhost'
});

server.start(() => {
  console.info('==> Server is listening.');
  console.info(`==> Go to http://${server.info.uri}:${server.info.port}`);
});

server.route({
  method:  'GET',
  path:    '/favicon.ico',
  handler: ( request, reply ) => {
    reply('');
  }
});

/**
 * Attempt to serve static requests from the public folder.
 */
server.route({
  method:  '*',
  path:    '/{params*}',
  handler: ( request, reply ) => {
    reply.file(request.path);
  }
});

/**
 * Catch dynamic requests here to fire-up React Router.
 */
server.ext('onPreResponse', ( request, reply ) => {
  if ( typeof request.response.statusCode !== 'undefined' ) {
    return reply.continue();
  }

  Router.run(routes, request.path, ( Handler, router ) => {
    let reactString = React.renderToString(
      <Handler data={data} />
    );
    let webserver   = process.env.NODE_ENV === 'production' ?
      '' : '//localhost:8080';

    reply(
      `<!doctype html>
      <html lang="en-us">
        <head>
          <meta charset="utf-8">
          <title>Test Runner</title>
        </head>
        <body>
          <div id="app">${reactString}</div>
          <script src="${webserver}/dist/client.js"></script>
        </body>
      </html>`
    );
  });
});
