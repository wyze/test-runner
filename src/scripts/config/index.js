import _data   from './data';
import _routes from './routes';

export const data = _data;
export const routes = _routes;
