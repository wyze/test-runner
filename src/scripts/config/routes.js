import React                   from 'react';
import { Route, DefaultRoute } from 'react-router';
import App                     from '../screens/app';
import Home                    from '../screens/home';

export default (
  <Route handler={App}>
    <Route name="home" path="/" handler={Home} />
    <DefaultRoute handler={Home} />
  </Route>
);
