import React            from 'react';
import Router           from 'react-router';
import { routes, data } from './config';

Router.run(
  routes,
  Router.HistoryLocation,
  Handler =>
    React.render(<Handler data={data} />, document.querySelector('#app'))
);

/**
 * Detect whether the server-side render has been discarded due to an invalid
 * checksum.
 */
if ( process.env.NODE_ENV !== 'production' ) {
  const reactRoot = window.document.querySelector('#app');

  if (
    !reactRoot || !reactRoot.firstChild || !reactRoot.firstChild.attributes ||
    !reactRoot.firstChild.attributes['data-react-checksum']
  ) {
    console.error(
      `Server-side React render was discarded.
       Make sure that your initial render does not contain any client-side code.
      `
    );
  }
}
