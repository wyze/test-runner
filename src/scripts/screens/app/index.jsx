if ( process.env.BROWSER === true ) {
  require('../../../styles/main.css');
}

import React            from 'react';
import { RouteHandler } from 'react-router';

export default class App extends React.Component {
  render() {
    return (
      <div>
        <RouteHandler {...this.props} />
      </div>
    );
  }
}
