import React, { PropTypes } from 'react';
import countBy              from 'lodash/collection/countBy';

export default class TestFooter extends React.Component {
  static propTypes = {
    tests: PropTypes.array.isRequired
  }

  render() {
    const len = this.props.tests.length;
    const statuses = countBy(this.props.tests, 'status');

    return (
      <footer>
        <div className="success">Passed: {statuses['true'] || 0}/{len}</div>
        <div className="failed">Failed: {statuses['false'] || 0}/{len}</div>
        <div>Running: {statuses['null'] || 0}/{len}</div>
        {!statuses['undefined'] && !statuses['null'] &&
          <div className="finished">FINISHED!</div>
        }
      </footer>
    );
  }
}
