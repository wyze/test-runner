import React, { PropTypes } from 'react';

export default class TestItem extends React.Component {
  static propTypes = {
    test: PropTypes.object.isRequired
  }

  state = {}

  componentWillReceiveProps = newProps => {
    if ( newProps.test.status === null ) {
      this.setState({ status: null });

      newProps.test.run(result => {
        this.setState({
          status: result
        });
      });
    }
  }

  render() {
    const { description } = this.props.test;
    const { status } = this.state;

    let statusText;

    if ( status === undefined ) {
      statusText = 'Not Started Yet';
    } else if ( status === null ) {
      statusText = 'Running';
    } else {
      statusText = status ? 'Passed' : 'Failed';
    }

    return (
      <div>
        <span>{description}</span>
        <span
          className={
            /^(P|F)/.test(statusText) && (status ? 'success' : 'failed')
          }
        >{statusText}</span>
      </div>
    );
  }
}
