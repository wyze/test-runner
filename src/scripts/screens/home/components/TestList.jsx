import React, { PropTypes } from 'react';
import TestItem             from './TestItem';
import TestFooter           from './TestFooter';

export default class TestList extends React.Component {
  static propTypes = {
    tests: PropTypes.array.isRequired
  }

  state = {
    tests: this.props.tests
  }

  handleClick = e => {
    e.preventDefault();

    this.setState({
      tests: this.state.tests.map(test => {
        (runner => {
          test.run(result => {
            this.setState({
              tests: this.state.tests.map(t =>
                t.description === runner.description ?
                  { ...runner, status: result } : t
              )
            });
          });
        }(test));

        return { ...test, status: null };
      })
    });
  }

  render() {
    const { tests } = this.state;

    return (
      <div>
        <button type="button" onClick={this.handleClick}>Run Tests</button>
        <div>
          {tests.map(test =>
            <TestItem
              test={test}
              key={test.description}
            />
          )}
        </div>
        <TestFooter tests={tests} />
      </div>
    );
  }
}
