import React, { PropTypes } from 'react';
import TestList             from './components/TestList';

export default class Home extends React.Component {
  static propTypes = {
    data: PropTypes.array.isRequired
  }

  render() {
    return (
      <div>
        <TestList tests={this.props.data} />
      </div>
    );
  }
}
